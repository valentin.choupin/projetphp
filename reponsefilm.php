<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Reponse a la demande</title>
<link rel="stylesheet"
		href="style.css">
<body>
	
<?php
	date_default_timezone_set('Europe/Paris');
try{
	date_default_timezone_set('Europe/Paris');
   
	echo "<h1>Voici votre requete</h1>";
	$file_db=new PDO('sqlite:film.sqlite3');
	$file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
	
	
	$statement=$file_db->prepare('Select * from film where NomFilm=:nomFilm');
	$statement->bindParam(':nomFilm',$_GET['nomFilm']);
	$statement->execute();
	
	while($result=$statement->fetch()){
		echo "<div>".$result['NomFilm'].' Sortie en : '.$result['annee'].' De type : '.$result['genre']."</div>";
	}
}
catch(PDOException $ex){
	  echo $ex->getMessage();
	  }
	
?>	 
</body>
</head>
</html>
