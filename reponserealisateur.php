<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Reponse a la demande</title>
<link rel="stylesheet"
		href="style.css">
<body>
	
<?php
	
try{
	date_default_timezone_set('Europe/Paris');
   
	echo "<h1><div>Voici votre requete</h1></div>";
	echo "<div>Les films realises par ".$_GET['NomRealisateur']." sont :</div>";
	$file_db=new PDO('sqlite:film.sqlite3');
	$file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
	
	
	$statement=$file_db->prepare('Select * from film where NomRealisateur=:NomRealisateur');
	$statement->bindParam(':NomRealisateur',$_GET['NomRealisateur']);
	$statement->execute();
	
	echo "<ul>";
	while($result=$statement->fetch()){
		echo "<li><div>".$result['NomFilm'].' '
		.$result['annee']."</li></div>\n";
	}
	echo "</ul>";
	$file_db=null;
}
catch(PDOException $ex){
	  echo $ex->getMessage();
	  }
	
?>	 
</body>
</head>
</html>
